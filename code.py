# CircuitPlaygroundExpress_CapTouch

import time
import neopixel
import board
import touchio
import array
import math
import audioio
import digitalio
pixels = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=1)
touch1 = touchio.TouchIn(board.A1)
touch2 = touchio.TouchIn(board.A2)
touch3 = touchio.TouchIn(board.A3)
touch4 = touchio.TouchIn(board.A4)
touch5 = touchio.TouchIn(board.A5)
touch6 = touchio.TouchIn(board.A6)
touch7 = touchio.TouchIn(board.A7)
FREQUENCY = 440  # 440 Hz middle 'A'
SAMPLERATE = 8000  # 8000 samples/second, recommended!

# Generate one period of sine wav.
length = SAMPLERATE // FREQUENCY
sine_wave = array.array("H", [0] * length)
for i in range(length):
    sine_wave[i] = int(math.sin(math.pi * 2 * i / 18) * (2 ** 15) + 2 ** 15)
# enable the speaker
speaker_enable = digitalio.DigitalInOut(board.SPEAKER_ENABLE)
speaker_enable.direction = digitalio.Direction.OUTPUT
speaker_enable.value = True

audio = audioio.AudioOut(board.A0)
sine_wave_sample = audioio.RawSample(sine_wave)
from digitalio import DigitalInOut, Direction, Pull

led = DigitalInOut(board.D13)
led.direction = Direction.OUTPUT

button = DigitalInOut(board.BUTTON_A)
button.direction = Direction.INPUT
button.pull = Pull.DOWN
while True:
    if button.value:  # button is pushed
        led.value = True
        audio.play(sine_wave_sample, loop=True)  # keep playing the sample over and over
        time.sleep(.7)  # until...
        audio.stop()
    time.sleep(0.01)
    if touch1.value:
        print("A1 touched!")
        '''for x in range(0 ,220,1):
        for p in range(0, 220, 1):'''
        for j in range(0, 220, 1):
            pixels.fill((j, 50, 50))
            pixels.show()
        audio.play(sine_wave_sample, loop=True)  # keep playing the sample over and over
        time.sleep(1)  # until...
        audio.stop()
        pixels.fill((0, 0, 0))
        time.sleep(1)

    if touch2.value:
        print("A2 touched!")
        pixels.fill((0, 225, 0))
        pixels.show()
        pixels.fill((0, 0, 0))
        time.sleep(1)

    if touch3.value:
        print("A3 touched!")
        pixels.fill((0, 0, 225))
        pixels.show()
        pixels.fill((0, 0, 0))
        time.sleep(1)

    if touch4.value:
        print("A4 touched!")
        pixels.fill((255, 255, 255))
        pixels.fill((0, 0, 0))
        time.sleep(1)

    if touch5.value:
        print("A5 touched!")
        pixels.fill((0, 0, 225))
        pixels.show()
        pixels.fill((0, 0, 0))
        pixels.show()
        audio.play(sine_wave_sample, loop=True)  # keep playing the sample over and over
        time.sleep(.5)  # until...
        audio.stop()
        audio.play(sine_wave_sample, loop=True)  # keep playing the sample over and over
        time.sleep(.2)  # until...
        audio.stop()
        audio.play(sine_wave_sample, loop=True)  # keep playing the sample over and over
        time.sleep(.5)  # until...
        audio.stop()
        audio.play(sine_wave_sample, loop=True)  # keep playing the sample over and over
        time.sleep(.3)  # until...
        audio.stop()
        pixels.fill((0, 0, 0))
        time.sleep(1)
    ''''if touch6.value:
        print("A6 touched!")
        for x in range(10):
            for y in range(1, .1):
                pixels1 = neopixel.NeoPixel(board.NEOPIXEL, 10, brightness=y)
                pixels1.fill((225, 0, 0))
                pixels1.show()
                pixels1.fill((0, 0, 0))
                time.sleep(1)''''

    if touch7.value:
        print("A7 touched!")
        pixels.fill((66, 120, 180))
        pixels.show()
        audio.play(sine_wave_sample, loop=True)  # keep playing the sample over and over
        time.sleep(.5)  # until...
        audio.stop()
        audio.play(sine_wave_sample, loop=True)  # keep playing the sample over and over
        time.sleep(.5)  # until...
        audio.stop()
        pixels.fill((0, 0, 0))
        time.sleep(1)

    time.sleep(.1)
